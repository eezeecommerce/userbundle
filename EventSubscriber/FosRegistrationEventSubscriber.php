<?php

namespace eezeecommerce\UserBundle\EventSubscriber;


use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Lexik\Bundle\MailerBundle\Message\MessageFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FosRegistrationEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var SettingsProvider
     */
    private $provider;

    /**
     * @var MessageFactory
     */
    private $mf;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_COMPLETED => array(
                "onRegistrationSuccess"
            )
        );
    }

    public function setSettings(SettingsProvider $provider)
    {
        $this->provider = $provider;
    }

    public function setMessageFactory(MessageFactory $mf)
    {
        $this->mf = $mf;
    }

    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function onRegistrationSuccess(FilterUserResponseEvent $event)
    {
        $route = $event->getRequest()->get("_route");
        if ($route == "registrationTrade") {

            $user = $event->getUser();

            $request = $event->getRequest();

            $settings = $this->provider->loadSettingsBySite();
            $to = $settings->getBusinessContactEmail();
            $params = array(
                "form" => $user,
                "subject" => "Trade Registration Request",
                "business_name" => $request->get("business_name"),
                "address_1" => $request->get("address_1"),
                "address_2" => $request->get("address_2"),
                "city" => $request->get("city"),
                "county" => $request->get("county"),
                "postcode" => $request->get("postcode"),
                "country" => $request->get("country"),
                "phone" => $request->get("phone"),
                "url" => $request->get("url"),
            );
            $locale = "en";

            $message = $this->mf->get('trade_registration', $to, $params, $locale);

            $this->mailer->send($message);
        }
    }
}