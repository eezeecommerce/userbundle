<?php

namespace eezeecommerce\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class AddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_first', null, [
                "label" => false,
            ])
            ->add('name_last', null, [
                "label" => false,
            ])
            ->add('address_1', null, [
                "label" => false,
            ])
            ->add('address_2', null, [
                "label" => false,
            ])
            ->add('city', null, [
                "label" => false,
            ])
            ->add('county', null, [
                "label" => false,
            ])
            ->add('postcode', null, [
                "label" => false,
            ])
            ->add('contact_email', null, [
                "label" => false,
            ])
            ->add('contact_phone', null, [
                "label" => false,
            ])
            ->add('country', 'entity', [
                'class' => "eezeecommerceShippingBundle:Country",
                'query_builder' => function (EntityRepository $em) {
                    return $em->createQueryBuilder('u')
                        ->orderBy('u.sort', 'ASC');
                },
                'property' => "name",
                "label" => false,
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\UserBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_userbundle_address';
    }
}
