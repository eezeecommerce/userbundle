<?php

namespace eezeecommerce\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserAdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name')
            ->add('last_name')
            ->add('company_name')
            ->add('notes')
            ->add('home_number')
            ->add('work_number')
            ->add('mobile_number')
            ->add('vat_number')
            ->add('override_vat')
            ->add('discount', "entity", array(
                'class' => "eezeecommerceDiscountBundle:Discounts",
                'query_builder' => function(EntityRepository $em) {
                    return $em->createQueryBuilder('d')
                        ->where("d.discount_method = :type")
                        ->setParameter("type",1);
                },
                'property' => "name",
                'label' => "Discount Structure",
                'required' => false
            ))
            ->add('grouping', "entity", array(
                "class" => "eezeecommerceUserBundle:Groups",
                "choice_label" => "name",
                "multiple" => true,
                "required" => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\UserBundle\Entity\User'
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
