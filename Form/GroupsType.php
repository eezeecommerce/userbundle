<?php

namespace eezeecommerce\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extenstion\Core\Type\CollectionType;

class GroupsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                "label" => false,
            ))
            ->add('pricing', "collection", array(
                "label" => false,
                'type' => new GroupsPricingType(),
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                "by_reference" => false,
                'prototype_name' => "__pricing__",
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\UserBundle\Entity\Groups'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "eezeecommerce_userbundle_groups";
    }

}
