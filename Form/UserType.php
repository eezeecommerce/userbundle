<?php

namespace eezeecommerce\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', null, array(
                "label" => "first.name",
                'translation_domain' => 'eezeecommerceUserBundle'
            ))
            ->add('last_name', null, array(
                "label" => "last.name",
                'translation_domain' => 'eezeecommerceUserBundle'
            ))
            ->add('company_name', null, array(
                "label" => "company.name",
                'translation_domain' => 'eezeecommerceUserBundle'
            ))
            ->add('home_number', null, array(
                "label" => "home.number",
                'translation_domain' => 'eezeecommerceUserBundle'
            ))
            ->add('work_number', null, array(
                "label" => "work.number",
                'translation_domain' => 'eezeecommerceUserBundle'
            ))
            ->add('mobile_number', null, array(
                "label" => "mobile.number",
                'translation_domain' => 'eezeecommerceUserBundle'
            ))
            ->add('vat_number', null, array(
                "label" => "vat.number",
                'translation_domain' => 'eezeecommerceUserBundle'
            ));
    }
    
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
