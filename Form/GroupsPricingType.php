<?php

namespace eezeecommerce\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class GroupsPricingType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("method", "choice", array(
                "choices" => array(
                    "ROUNDUP" => "Round Up",
                    "ROUNDDOWN" => "Round Down",
                    "ADD" => "Add",
                    "SUBTRACT" => "Subtract",
                    "MULTIPLY" => "Multiply",
                    "DIVIDE" => "Divide"
                )
            ))
            ->add("amount", null);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\UserBundle\Entity\GroupsPricing'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "eezeecommerce_userbundle_groupspricing";
    }

}
