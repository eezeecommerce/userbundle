<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="eezeecommerce\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=50)
     */
    protected $first_name;
    
    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string",length=50)
     */
    protected $last_name;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $company_name;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;
    
    /**
     * @ORM\Column="string", length=50, nullable=true)
     */
    protected $home_number;
    
    /**
     * @ORM\Column="string", length=50, nullable=true)
     */
    protected $work_number;
    
    /**
     * @ORM\Column="string", length=50, nullable=true)
     */
    protected $mobile_number;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $vat_number;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $override_vat;
    
    /**
     * @ORM\OneToMany(targetEntity="\eezeecommerce\OrderBundle\Entity\Orders", mappedBy="user")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="Address", mappedBy="user")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="\eezeecommerce\DiscountBundle\Entity\Discounts", inversedBy="user")
     * @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     */
    private $discount;
    
    /**
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\UserBundle\Entity\Groups", inversedBy="users")
     * @ORM\JoinTable(name="users_grouping")
     */
    protected $grouping;

    public function __construct()
    {
        parent::__construct();
        $this->orders = new ArrayCollection();
        $this->grouping = new ArrayCollection();
        $this->override_vat = false;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return User
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set homeNumber
     *
     * @param string $homeNumber
     *
     * @return User
     */
    public function setHomeNumber($homeNumber)
    {
        $this->home_number = $homeNumber;

        return $this;
    }

    /**
     * Get homeNumber
     *
     * @return string
     */
    public function getHomeNumber()
    {
        return $this->home_number;
    }

    /**
     * Set workNumber
     *
     * @param string $workNumber
     *
     * @return User
     */
    public function setWorkNumber($workNumber)
    {
        $this->work_number = $workNumber;

        return $this;
    }

    /**
     * Get workNumber
     *
     * @return string
     */
    public function getWorkNumber()
    {
        return $this->work_number;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     *
     * @return User
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobile_number = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->mobile_number;
    }

    /**
     * Set vatNumber
     *
     * @param string $vatNumber
     *
     * @return User
     */
    public function setVatNumber($vatNumber)
    {
        $this->vat_number = $vatNumber;

        return $this;
    }

    /**
     * Get vatNumber
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vat_number;
    }

    /**
     * Set overrideVat
     *
     * @param boolean $overrideVat
     *
     * @return User
     */
    public function setOverrideVat($overrideVat)
    {
        $this->override_vat = $overrideVat;

        return $this;
    }

    /**
     * Get overrideVat
     *
     * @return boolean
     */
    public function getOverrideVat()
    {
        return $this->override_vat;
    }

    /**
     * Add order
     *
     * @param \eezeecommerce\OrderBundle\Entity\Orders $order
     *
     * @return User
     */
    public function addOrder(\eezeecommerce\OrderBundle\Entity\Orders $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \eezeecommerce\OrderBundle\Entity\Orders $order
     */
    public function removeOrder(\eezeecommerce\OrderBundle\Entity\Orders $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add address
     *
     * @param \eezeecommerce\UserBundle\Entity\Address $address
     *
     * @return User
     */
    public function addAddress(\eezeecommerce\UserBundle\Entity\Address $address)
    {
        $this->address[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \eezeecommerce\UserBundle\Entity\Address $address
     */
    public function removeAddress(\eezeecommerce\UserBundle\Entity\Address $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set discount
     *
     * @param \eezeecommerce\DiscountBundle\Entity\Discounts $discount
     *
     * @return User
     */
    public function setDiscount(\eezeecommerce\DiscountBundle\Entity\Discounts $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return \eezeecommerce\DiscountBundle\Entity\Discounts
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Add grouping
     *
     * @param \eezeecommerce\UserBundle\Entity\Groups $grouping
     *
     * @return User
     */
    public function addGrouping(\eezeecommerce\UserBundle\Entity\Groups $grouping)
    {
        $this->grouping[] = $grouping;

        $grouping->addUser($this);

        return $this;
    }

    /**
     * Remove grouping
     *
     * @param \eezeecommerce\UserBundle\Entity\Groups $grouping
     */
    public function removeGrouping(\eezeecommerce\UserBundle\Entity\Groups $grouping)
    {
        $this->grouping->removeElement($grouping);
    }

    /**
     * Get grouping
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrouping()
    {
        return $this->grouping;
    }
}
