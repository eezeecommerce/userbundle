<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="groups_pricing")
 * @ORM\Entity(repositoryClass="eezeecommerce\UserBundle\Entity\GroupsPricingRepository")
 */
class GroupsPricing
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
       
    /**
     *@ORM\Column(type="string", columnDefinition="ENUM('ROUNDUP', 'ROUNDDOWN', 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE')", nullable=true)
     */
    protected $method;
    
    /**
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $amount;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $sort;
    
    /**
     * @ORM\ManyToMany(targetEntity="eezeecommerce\UserBundle\Entity\Groups", mappedBy="pricing")
     */
    protected $groups;
    
    
    
    
    
    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method
     *
     * @param string $method
     *
     * @return GroupsPricing
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return GroupsPricing
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Add group
     *
     * @param \eezeecommerce\UserBundle\Entity\Groups $group
     *
     * @return GroupsPricing
     */
    public function addGroup(\eezeecommerce\UserBundle\Entity\Groups $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \eezeecommerce\UserBundle\Entity\Groups $group
     */
    public function removeGroup(\eezeecommerce\UserBundle\Entity\Groups $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return GroupsPricing
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }
}
