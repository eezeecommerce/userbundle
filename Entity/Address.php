<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="eezeecommerce\UserBundle\Entity\AddressRepository")
 */
class Address 
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="First Name cannot be left blank")
     */ 
    protected $name_first;
    
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    protected $name_last;
    
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    protected $address_1;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $address_2;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    protected $city;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $county;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    protected $postcode;
    
    /**
     * @ORM\Column(type="string", length=125)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $contact_email;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    protected $contact_phone;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $billing_primary = null;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $shipping_primary = null;
    
    /**
     * @ORM\ManyToOne(targetEntity="\eezeecommerce\ShippingBundle\Entity\Country")
     * @ORM\JoinColumn(name="billing_country_id", referencedColumnName="id")
     */
    protected $country;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="address", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\OrderBundle\Entity\Orders", mappedBy="billing_address")
     */
    private $orders_billing_address;

    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\OrderBundle\Entity\Orders", mappedBy="shipping_address")
     */
    private $orders_shipping_address;


    /**
     * Address constructor.
     */
    public function __construct()
    {
        $this->orders_billing_address = new ArrayCollection();
        $this->orders_shipping_address = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nameFirst
     *
     * @return string
     */
    public function getNameFirst()
    {
        return $this->name_first;
    }

    /**
     * Set nameFirst
     *
     * @param string $nameFirst
     *
     * @return Address
     */
    public function setNameFirst($nameFirst)
    {
        $this->name_first = $nameFirst;

        return $this;
    }

    /**
     * Get nameLast
     *
     * @return string
     */
    public function getNameLast()
    {
        return $this->name_last;
    }

    /**
     * Set nameLast
     *
     * @param string $nameLast
     *
     * @return Address
     */
    public function setNameLast($nameLast)
    {
        $this->name_last = $nameLast;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address_1;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return Address
     */
    public function setAddress1($address1)
    {
        $this->address_1 = $address1;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address_2;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Address
     */
    public function setAddress2($address2)
    {
        $this->address_2 = $address2;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set county
     *
     * @param string $county
     *
     * @return Address
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Address
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get country
     *
     * @return \eezeecommerce\ShippingBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Country $country
     *
     * @return Address
     */
    public function setCountry(\eezeecommerce\ShippingBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get user
     *
     * @return \eezeecommerce\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \eezeecommerce\UserBundle\Entity\User $user
     *
     * @return Address
     */
    public function setUser(\eezeecommerce\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get billingPrimary
     *
     * @return boolean
     */
    public function getBillingPrimary()
    {
        return $this->billing_primary;
    }

    /**
     * Set billingPrimary
     *
     * @param boolean $billingPrimary
     *
     * @return Address
     */
    public function setBillingPrimary($billingPrimary)
    {
        $this->billing_primary = $billingPrimary;

        return $this;
    }

    /**
     * Get shippingPrimary
     *
     * @return boolean
     */
    public function getShippingPrimary()
    {
        return $this->shipping_primary;
    }

    /**
     * Set shippingPrimary
     *
     * @param boolean $shippingPrimary
     *
     * @return Address
     */
    public function setShippingPrimary($shippingPrimary)
    {
        $this->shipping_primary = $shippingPrimary;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contact_email;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     *
     * @return Address
     */
    public function setContactEmail($contactEmail)
    {
        $this->contact_email = $contactEmail;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     *
     * @return Address
     */
    public function setContactPhone($contactPhone)
    {
        $this->contact_phone = $contactPhone;

        return $this;
    }
}
