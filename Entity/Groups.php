<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="groups")
 * @ORM\Entity(repositoryClass="eezeecommerce\UserBundle\Entity\GroupsRepository")
 */
class Groups
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\UserBundle\Entity\User", mappedBy="grouping")
     */
    protected $users;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;
    
    /**
     * @ORM\ManyToMany(targetEntity="eezeecommerce\UserBundle\Entity\GroupsPricing", inversedBy="groups", cascade={"persist"})
     * @ORM\JoinTable(name="groups_groups_pricing")
     */
    protected $pricing;
    
    
    
    public function __construct()
    {
        $this->pricing = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Groups
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add user
     *
     * @param \eezeecommerce\UserBundle\Entity\User $user
     *
     * @return Groups
     */
    public function addUser(\eezeecommerce\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \eezeecommerce\UserBundle\Entity\User $user
     */
    public function removeUser(\eezeecommerce\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add pricing
     *
     * @param \eezeecommerce\UserBundle\Entity\GroupsPricing $pricing
     *
     * @return Groups
     */
    public function addPricing(\eezeecommerce\UserBundle\Entity\GroupsPricing $pricing)
    {
        $this->pricing[] = $pricing;

        return $this;
    }

    /**
     * Remove pricing
     *
     * @param \eezeecommerce\UserBundle\Entity\GroupsPricing $pricing
     */
    public function removePricing(\eezeecommerce\UserBundle\Entity\GroupsPricing $pricing)
    {
        $this->pricing->removeElement($pricing);
    }

    /**
     * Get pricing
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPricing()
    {
        return $this->pricing;
    }
}
