<?php

namespace eezeecommerce\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class eezeecommerceUserBundle extends Bundle
{
    
    public function getParent()
    {
        return "FOSUserBundle";
    }
    
}
